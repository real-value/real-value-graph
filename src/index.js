import Debug from 'debug'
const debug = Debug('graph')

// associates nodes with its ancestors and descendants via links
export function deriveLineage (graph) {
 const sourceNodes = (n) => graph.links.filter(l => l.target === n.id).map(l => graph.nodes.find(n => n.id === l.source))
 const targetNodes = (n) => graph.links.filter(l => l.source === n.id).map(l => graph.nodes.find(n => n.id === l.target))

 const ancestorNodes = (nd) => {
  if (nd.ancestors === undefined) {
   nd.ancestors = {}
  }
  let sNodes = sourceNodes(nd)
  if (sNodes.length === 0) { return nd }

  sNodes.forEach(sn => {
   if (nd.ancestors[sn.id] === undefined) {
    nd.ancestors[sn.id] = sn
    ancestorNodes(sn)
    Object.values(sn.ancestors).forEach(an => {
     if (an.id !== nd.id) { nd.ancestors[an.id] = an }
    })
   }
  })

  return nd.ancestors
 }

 const descendantNodes = (nd) => {
  if (nd.descendants === undefined) {
   nd.descendants = {}
  }
  let sNodes = targetNodes(nd)
  if (sNodes.length === 0) { return nd }

  sNodes.forEach(sn => {
   if (nd.descendants[sn.id] === undefined) {
    nd.descendants[sn.id] = sn
    descendantNodes(sn)
    Object.values(sn.descendants).forEach(an => {
     if (an.id !== nd.id) { nd.descendants[an.id] = an }
    })
   }
  })

  return nd.descendants
 }

 graph.nodes.forEach(n => {
  ancestorNodes(n)
  descendantNodes(n)
 })

 graph.nodes.forEach(n => {
  n.ancestorsCount = Object.keys(n.ancestors).length
  n.descendantsCount = Object.keys(n.descendants).length
 })

 return graph
}

export function surfaceLinks (graph) {
 const linkId = l => `${l.source}-${l.target}`
 let linksMap = graph.links.reduce((h, l) => { h[linkId(l)] = l; return h }, {})
 let newLinksCount = 0
 do {
  newLinksCount = 0
  let currentLinks = Object.values(linksMap)
  for (let i = 0; i < currentLinks.length; i++) {
   let l = currentLinks[i]
   let source = graph.nodes.find(n => n.id === l.source)
   let target = graph.nodes.find(n => n.id === l.target)
   if (source && target) {
    let sourceParent = graph.nodes.find(n => n.id === source.parentId)
    let targetParent = graph.nodes.find(n => n.id === target.parentId)
    if (sourceParent && targetParent) {
     if (sourceParent.id !== target.parentId) {
      let newLink = { source: sourceParent.id, target: targetParent.id, relationship: 'association' }
      let newLinkId = linkId(newLink)
      if (!linksMap[newLinkId]) {
       newLink.id = newLinkId
       linksMap[newLinkId] = newLink
       newLinksCount++
      }
     }
    }
   }
  }
 } while (newLinksCount > 0)
 return {
  nodes: graph.nodes,
  links: Object.values(linksMap)
 }
}

export function deriveLevel (graph) {
  const parent = n => graph.nodes.find(p => n.parentId === p.id)

  const setLevel = n => {
    if (n.level) return n.level
    let p = parent(n)
    if (!p) {
      n.level = 1
      return n.level
    }
    let parentLevel = setLevel(p)
    n.level = parentLevel + 1
  }

  graph.nodes.forEach(n => {
    setLevel(n)
  })

  return graph
}
