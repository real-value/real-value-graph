import Debug from 'debug'
import { describe } from 'riteway'
import { deriveLineage, surfaceLinks, deriveLevel } from '../src/index.js'
const debug = Debug('test')

describe('Enrich Ancestors and Descendants', async (assert) => {
  try {
    let newGraph = null

    let nodes = [{
      id: 1
    }]
    let links = []

    newGraph = deriveLineage({ nodes, links })

    assert({
      given: 'node with no links',
      should: 'be have zero ancestor/descendants',
      actual: `${nodes[0].ancestorsCount} ${nodes[0].descendantsCount}`,
      expected: '0 0'
    })

    nodes = [{ id: 1 }, { id: 2 }, { id: 3 }]
    links = [{ key: '1-2', source: 1, target: 2 }, { key: '2-3', source: 2, target: 3 }]

    newGraph = deriveLineage({ nodes, links })

    assert({
      given: 'node with source and descentant links',
      should: 'be have one ancestor/descendant',
      actual: `${nodes[1].ancestorsCount} ${nodes[1].descendantsCount}`,
      expected: '1 1'
    })

    assert({
      given: 'node with multi level source/target links',
      should: 'have appropriate ancestor/descendant',
      actual: `${nodes[2].ancestorsCount} ${nodes[0].descendantsCount}`,
      expected: '2 2'
    })

    nodes = [{ id: 1 }, { id: 2 }, { id: 3 }]
    links = [{ key: '1-2', source: 1, target: 2 }, { key: '2-3', source: 2, target: 3 }, { key: '3-1', source: 3, target: 1 }]

    newGraph = deriveLineage({ nodes, links })

    assert({
      given: 'node with source and descentant links',
      should: 'be have one ancestor/descendant',
      actual: `${Object.keys(nodes[0].ancestors).length} ${Object.keys(nodes[0].descendants).length}`,
      expected: '2 2'
    })
} catch (ex) {
  debug(ex)
}
})

describe('surface links', async (assert) => {
  try {
    let nodes = [{ id: 1 }]
    let links = []

    let newGraph = surfaceLinks({ nodes, links })

    assert({
      given: 'a node with no links',
      should: 'be surface no links',
      actual: `${newGraph.links.length}`,
      expected: '0'
    })

    nodes = [{ id: 1 }, { id: 2 }]
    links = [{ source: 1, target: 2 }]

    newGraph = surfaceLinks({ nodes, links })

    assert({
      given: '2 nodes and 1 link',
      should: 'not be changed ',
      actual: `${newGraph.links.length}`,
      expected: '1'
    })

    nodes = [{ id: 1 }, { id: 2 }, { id: 10, parentId: 1 }, { id: 20, parentId: 2 }]
    links = [{ source: 10, target: 20 }]

    newGraph = surfaceLinks({ nodes, links })

    assert({
      given: '2 parents each with a single child and link at the child level',
      should: 'be a link at the parent level',
      actual: `${newGraph.links.length}`,
      expected: '2'
    })

    nodes = [{ id: 1 }, { id: 2 }, { id: 10, parentId: 1 }, { id: 20, parentId: 1 }]
    links = [{ source: 10, target: 20 }]

    newGraph = surfaceLinks({ nodes, links })

    assert({
      given: '1 parents with 2 children ',
      should: 'be no additional link',
      actual: `${newGraph.links.length}`,
      expected: '1'
    })

    nodes = [{ id: 1 },
                { id: 10, parentId: 1 }, { id: 11, parentId: 1 },
                  { id: 100, parentId: 10 }, { id: 101, parentId: 10 }, { id: 110, parentId: 11 }, { id: 111, parentId: 11 },
              { id: 2 },
                { id: 20, parentId: 2 }, { id: 21, parentId: 2 },
                  { id: 200, parentId: 20 }, { id: 201, parentId: 20 }, { id: 210, parentId: 21 }, { id: 211, parentId: 21 }
              ]
    links = [{ source: 100, target: 200 }]

    newGraph = surfaceLinks({ nodes, links })

    assert({
      given: '1 parents with 2 children ',
      should: 'be no additional link',
      actual: `${newGraph.links.length}`,
      expected: '3'
    })

    links = [{ source: 100, target: 110 }, { source: 110, target: 200 }, { source: 200, target: 210 }]

    newGraph = surfaceLinks({ nodes, links })

    assert({
      given: '1 parents with 2 children ',
      should: 'be no additional link',
      actual: `${newGraph.links.length}`,
      expected: '7'
    })

    links = [{ source: 100, target: 110 },
       { source: 10, target: 20 }
      ]

    newGraph = deriveLevel({ nodes, links })
    newGraph = surfaceLinks(newGraph)

    assert({
      given: '1 parents with 2 children ',
      should: 'be no additional link',
      actual: `${newGraph.links.length}`,
      expected: '4'
    })
  } catch (ex) {
    debug(ex)
  }
})

describe('deriveLevel', async (assert) => {
  try {
    let nodes = [{ id: 1 }]
    let links = []

    let newGraph = deriveLevel({ nodes, links })

    assert({
      given: 'a node with no links',
      should: 'have level 1',
      actual: `${newGraph.nodes[0].level}`,
      expected: '1'
    })

    nodes = [{ id: 1 }, { id: 2, parentId: 1 }]

    newGraph = deriveLevel({ nodes, links })

    assert({
      given: 'child node',
      should: 'have depth 2',
      actual: `${newGraph.nodes[1].level}`,
      expected: '2'
    })

    nodes = [{ id: 1 }, { id: 2, parentId: 1 }, { id: 3, parentId: 2 }]

    newGraph = deriveLevel({ nodes, links })

    assert({
      given: 'root with child with child',
      should: 'be a link at the parent level',
      actual: `${newGraph.nodes.map(n => n.level).join(' ')}`,
      expected: '1 2 3'
    })
  } catch (ex) {
    debug(ex)
  }
})
