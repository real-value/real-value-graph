# Graph Library     

A library for operating on graphs.

A graph is a data structure having
```
{
    nodes: [ { id: <string>, parentId: <string>},...]
    links: [ { source: <string>, target: <string>, relationship: <string>},...]
}
```

This data structure is intended to representation a `system`.

> The fundamental concepts or properties of a system in its environment embodied in its elements, relationships, and in the principles of its design and evolution
— ISO/IEC/IEEE 42010:2011


## About

What types of things do we want to perform on this graph structure.

### Derive Lineage

This operation enriches each nodes with all node reachable from the node via inbound and outbound links

```
{
    nodes[{id:1},{id:2},{id:3}]
    links:[{source:1,target:2},{source:2,target:3},{source:3,target:1}]
}
```
will produce
```
{
    nodes[
        {id:1,ancestorsCount: 2, ancestors:{2: {id:2},3:{id:3}},descendantsCount: 2, descendants:{2: {id:2},3:{id:3}},
        ...
    links:[...]
}
```

### Surface Links

This operation raise links between tree nodes to parent nodes

```
{
    nodes[{id:1},{id:2},{id:10,parentId: 1},{id:20:parentId:2}]
    links:[{source:10,target:20}]
}
```
will produce
```
{
    nodes[...]
    links:[{source:10,target:20},{source:1,target:2}]
}
```

The idea here is that there can be interconnections at lower levels of detail that result in logical connections at higher level of abstraction


## Install

```
npm install 2020-10-02-RealValueGraph
yarn install 2020-10-02-RealValueGraph
```

## How to use

